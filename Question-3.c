/* Wickramaarachchi H.P.
	Student no-19020902*/

//program to add and multiply two given matrixes

#include<stdio.h>

void element(int [10][10],int,int);
//function to input elements of matrixes
void addition(int [10][10],int [10][10],int,int);
//function to add two matrixes
void multiplication(int [10][10],int [10][10],int,int,int);
//function to multiply two matrixes
void display(int [10][10],int,int);


void element(int first[10][10],int r,int c)
{
    int i,j;
    for(i=0;i<r;i++)
    {
       for(j=0;j<c;j++)
       {
         scanf("%d",&first[i][j]);
       }
    }
}

void addition(int first[10][10],int second[10][10],int row1,int col1)
{
  int i,j;
  int add[10][10];
  for(i=0;i<row1;i++)
  {
    for(j=0;j<col1;j++)
    {
       add[i][j]=first[i][j]+second[i][j];
    }
  }
  printf("Addition of two matrixes:\n");
  display(add,row1,col1);
}

void multiplication(int first[10][10],int second[10][10],int row1,int col1,int col2)
{
   int i,j,mul[10][10],k;
   for(i=0;i<row1;i++)
   {
      for(j=0;j<col2;j++)
      {
        mul[i][j]=0;
         for(k=0;k<col1;k++)
         {
            mul[i][j]=mul[i][j]+(first[i][k]*second[k][j]);
         }
      }
    }
   printf("Multiplication of two matrixes:\n");
   display(mul,row1,col2);
}

void display(int first[10][10],int r,int c)
{
    int i,j;
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d\t",first[i][j]);
         }
    printf("\n\n");
    }
}

int main()
{
   int first[10][10],second[10][10];
   int row1,row2,col1,col2;
   
   
   printf("Enter no of rows and columns in first matrix:\n");
   scanf("%d %d",&row1,&col1);
   printf("\n");
  
   printf("Enter no of rows and columns in second matrix:\n");
   scanf("%d %d",&row2,&col2);
   printf("\n");
   
   printf("Enter elements of 1st matrix\n");
   element(first,row1,col1);
   printf("\n");
   
   printf("Enter elements of 2nd matrix\n");
   element(second,row2,col2);
   printf("\n");
   
   if((row1==row2)&&(col1==col2))
     addition(first,second,row1,col1);
   else
     printf("Addition not possible\n");
     
   if(col1==row2)
     multiplication(first,second,row1,col1,col2);
    else
      printf("Multiplication not possible");
   
}


