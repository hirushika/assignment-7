/* Wickramaarachchi H.P.
	Student no-19020902*/

//program to check frequancy of a letter in given sentence

#include <stdio.h>
#include <string.h>

int main()
{
    char string[1000],letter;
    int frequency = 0,i;
    
    puts("Enter a string : ");
    gets(string);
    
    puts("\nEnter your character :");
    letter = getchar();
    
    for (i = 0; i < strlen(string); i++) {
        if (letter == string[i]) {
            frequency++;
        }
    }
    printf("\nFrequency of letter %c in %s is : %d \n", letter, string, frequency);
    
    return 0;
}

