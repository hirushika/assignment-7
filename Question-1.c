/* Wickramaarachchi H.P.
	Student no-19020902*/

//program to reverse a sentence entered by user

# include <stdio.h>
# include <string.h>

int main() 
{
    char sentence[1000];
    int x;
    
    puts("Enter a sentence (Number of characters should less than 1000) :\n");
    gets(sentence);
    
    printf("\nReversed sentence :\n\n");
    
    for (x = strlen(sentence)-1; x >= 0; x--) {
        printf("%c", sentence[x]);
    }
    printf("\n");
    
    return 0;
} 


